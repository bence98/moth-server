# Moth web server

It's a prototype web server written on C.

Repo: <https://gitlab.com/adrianrobotka/moth-server>

## Doxygen
To generate documentation run `doxygen` command and then
open the `docs/html/index.html` file.

## Compile
Run `cmake .` command then `make`.
If you are brave you can run `make install` too.
Output of the build will may be placed in the `cmake-build-debug` folder.

## User documentation
Run `moth-server` binary. That's it.
The first optional parameter of the program is the path of the config file.
All path is relative to the PWD, so be careful.
Log entries are handled by syslog. Only fatal startup messages will be printed to stderr.
SIGHUP will terminate the process properly.

## Programmers' documentation
Once upon a time there was a doxygen documentation generator...
If you don't believe it, go after it.

## Memory leak
I've used `valgrind` to find and fix memory leaks.

## Task specification
- Feladatkiírás: `docs/feladatkiírás.md`
- Specifikáció: `docs/specifikáció.pdf`

## My sources
- RFC 2616: <https://tools.ietf.org/html/rfc2616.html>
- Hint to begin: <https://blog.abhijeetr.com/2010/04/very-simple-http-server-writen-in-c.html>
- Icon is from www.flaticon.com (licensed by CC 3.0 BY)