/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Static and runtime configuration (implementation)
 *
 * Compile-time constants, runtime config struct and related methods
 *
 */

#include "config.h"

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/**
 * Possible states of config parsing
 */
typedef enum config_parse_states {
    /// Before a line starts
            PRE_ENTRY,

    /// Read value of a key
            READ_KEY,

    /// Read key-value separator
            READ_KEY_SEPARATOR,

    /// Read a config value
            READ_VALUE,

    /// Read a comment line
            COMMENT,
} config_parse_states;

/**
 * Get value pointer by config key's name
 *
 * @param key Key string
 * @param config Initialized prg_config struct
 * @return Config value pointer
 */
char **get_value_pointer(const char *key, prg_config *config) {
    if (strcmp(key, "port") == 0)
        return &config->port;
    else if (strcmp(key, "webroot") == 0)
        return &config->webroot;
    else if (strcmp(key, "reuse") == 0)
        return &config->do_reuse;
    else
        return NULL;
}

char *read_config_file(const char *filename, prg_config *config) {
    FILE *file = fopen(filename, "r");

    if (file == NULL)
        return "cannot open config file for read";

    size_t len = 0; // can be dangerous (has some 0 assignment)
    char key[CONFIG_KEY_BUFFER_LENGTH];

    char value_buffer[CONFIG_VALUE_BUFFER_LENGTH];
    char **value_ptr = NULL;

    config_parse_states state = PRE_ENTRY;
    char c;
    bool repeat_cycle = false;
    while (repeat_cycle || (c = (char) fgetc(file)) != EOF) {
        repeat_cycle = false;

        if (state != COMMENT && !isalnum(c) && !(
                c == '#' ||
                c == '\n' ||
                c == '\t' ||
                c == ':' ||
                c == '.' ||
                c == '/' ||
                c == '-' ||
                c == '_' ||
                c == ' '))
            return "config string contains illegal character";;

        switch (state) {
            case PRE_ENTRY:
                len = 0;
                if (c == '#')
                    state = COMMENT;
                else if (c == '\n' || c == ' ')
                    state = PRE_ENTRY;
                else {
                    repeat_cycle = true;
                    state = READ_KEY;
                }
                break;
            case READ_KEY:
                if (c == ':') {
                    value_ptr = get_value_pointer(key, config);
                    if (value_ptr == NULL)
                        return "config key is not legal";

                    state = READ_KEY_SEPARATOR;
                } else if (len >= CONFIG_KEY_BUFFER_LENGTH - 1)
                    return "config key is larger then CONFIG_KEY_BUFFER_LENGTH";
                else {
                    key[len++] = c;
                    key[len] = '\0';
                }
                break;
            case READ_KEY_SEPARATOR:
                if (c == ' ' || c == '\t')
                    continue;
                else {
                    len = 0;
                    repeat_cycle = true;
                    state = READ_VALUE;
                }
                break;
            case READ_VALUE:
                if (c == '\n') {
                    size_t total_value_len = strlen(value_buffer);
                    *value_ptr = malloc(sizeof(char) * total_value_len + 1);
                    strcpy(*value_ptr, value_buffer);
                    (*value_ptr)[len] = '\0';
                    state = PRE_ENTRY;
                } else if (len >= CONFIG_VALUE_BUFFER_LENGTH - 1)
                    return "config value is larger than CONFIG_VALUE_BUFFER_LENGTH";
                else
                    value_buffer[len++] = c;
                value_buffer[len] = '\0';
                break;
            case COMMENT:
                if (c == '\n')
                    state = PRE_ENTRY;
                break;
        }
    }

    fclose(file);
    return NULL;
}

void free_config(prg_config const *config) {
    free(config->webroot);
    free(config->port);
}
