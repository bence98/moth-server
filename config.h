/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Static and runtime configuration
 *
 * Compile-time constants, runtime config struct and related methods
 *
 */

#ifndef MOTH_SERVER_CONFIG_H
#define MOTH_SERVER_CONFIG_H

#include "storage.h"

/// Max length of the URL's path part (by rule of thumb)
#define MAX_PATH_LENGTH 2000

/// Max length of a header string
#define HEADER_BUFFER_LENGTH 1000

/// Length of request buffer
#define REQUEST_BUFFER_LENGTH 99999

/// Max length of a config key
#define CONFIG_KEY_BUFFER_LENGTH 32

/// Max length of a config value (e.g. path)
#define CONFIG_VALUE_BUFFER_LENGTH 255

/// Default 404 error page's path
#define DEFAULT_ERROR_404_FILE "/error/404.html"

/**
 * Runtime configuration of the program.
 * This will be filled by init of the program.
 */
typedef struct prg_config {
    /// Port string of the socket
    char *port;

    /// Public folder of the server (relative to the PWD)
    char *webroot;
    
    /// Is address reuse allowed?
    char* do_reuse;
} prg_config;

/**
 * Parse config file
 *
 * @param filename Path of the config file
 * @param config Config struct to write values
 * @return Fatal error message, NULL if success
 */
char *read_config_file(const char *filename, prg_config *config);

/**
 * Free runtime configuration
 *
 * @param config Config instance
 */
void free_config(prg_config const *config);

#endif //MOTH_SERVER_CONFIG_H
