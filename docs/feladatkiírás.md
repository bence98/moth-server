# Feladatkiírás

Az általam választott feladat nem volt benne a 
[feladatlistában](https://infoc.eet.bme.hu/nhflista/).
Egy webszerver programot valósítok meg. 
Parancssorról lehet vezérelni, 
képes fogadni szabványos HTTP/1.1 lekérdezéseket 
és szintén szabványos válaszokat készít a lekérdezésekre.
Az URL path része alapján a fájlrendszerről beolvas egy fájlt és választ készít belőle.
A hozzáférési naplót a szabványos kimeneten jeleníti meg.