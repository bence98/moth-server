/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief HTTP response/request structs
 *
 * Terminologically, most part of this file is derived from RFC 2616.
 *
 */

#ifndef MOTH_SERVER_HTTP_STRUCTS_H
#define MOTH_SERVER_HTTP_STRUCTS_H

#include "config.h"

/**
 * HTTP request version
 */
typedef enum http_version {
    HTTP_VER_09,
    HTTP_VER_10,
    HTTP_VER_11
} http_version;

/**
 * HTTP request method
 */
typedef enum http_method {
    HTTP_GET
} http_method;

/**
 * HTTP request
 */
typedef struct http_request {
    /// HTTP method
    http_method method;

    /// Absolute path (started by '/'). E.g. /folder/file.attr
    char abs_path[MAX_PATH_LENGTH];

    /// Index of the last dot in the abs_path (-1 is the default)
    int last_dot_in_path;

} http_request;

/**
 * HTTP response
 */
typedef struct http_response {
    /// HTTP-Version header
    http_version version;

    /// Status line (code and message)
    const char *status_line;

    /// Content-Type header
    const char *content_type;

    /// Length of the body (in C's char)
    size_t body_length;

    /// Body of the response
    const char *body;
} http_response;

#endif //MOTH_SERVER_HTTP_STRUCTS_H
