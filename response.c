/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Response logic (implementation)
 *
 * Factory of HTTP responses. It's an abstraction in a way.
 *
 */

#include "response.h"

#include<string.h>
#include<stdlib.h>

file_storage_entry const *storage;
size_t storage_size;

/// fallback 404 error page (source of internal HTML page)
char *fallback_404_page = "<!DOCTYPE html>\n"
                          "<html>\n"
                          "<head>\n"
                          "   <title>404</title>\n"
                          "</head>\n"
                          "<body>\n"
                          "   <h1>Error 404</h1>\n"
                          "   <p>No content.</p>\n"
                          "   <p><pre>Served by Moth</pre></p>\n"
                          "</body>\n"
                          "</html>\n";

/**
 * Mime-types
 */
typedef struct mime_type {
    /// File attribute to obtain mime type
    const char *attr;

    /// Mime-type text
    const char *mime;
} mime_type;

/// Fallback Mime-type
const char *default_mime = "text/plain";

/// Mime-type of error pages
const char *html_mime = "text/html";

/// Built-in mime-type list
mime_type mime_types[] = {
        {".html", "text/html"},
        {".css",  "text/css"},
        {".js",   "text/javascript"},
        {".json", "application/json"},
        {".png",  "image/png"},
        {".jpeg", "image/jpeg"},
};

/// Length of built-in mime-type list
const size_t mime_types_len = sizeof(mime_types) / sizeof(mime_types[0]);

/**
 * Comperer of a string and a file_storage_entry
 *
 * @param path_ptr Path to compare with
 * @param entry_ptr Element of an ordered array
 * @return Order
 */
int comp(const void *path_ptr, const void *entry_ptr) {
    char const *path = path_ptr;
    file_storage_entry const *entry = entry_ptr;

    return strcmp(path, entry->path);
}

/**
 * Lookup file from the storage
 *
 * @param path Path of the file
 * @return File entry pointer; NULL if cannot find
 */
const file_storage_entry *lookup_entry(const char *path) {
    return bsearch(path,
                   storage,
                   storage_size,
                   sizeof(file_storage_entry),
                   comp);
}

void process_request(const http_request *request, http_response *response) {
    const char *ok = "200 OK";
    const char *not_found = "404 NOT FOUND";

    // Fill required elements of the struct
    response->version = HTTP_VER_11;
    response->content_type = default_mime;

    const file_storage_entry *file;
    size_t path_len = strlen(request->abs_path);

    // URL path ends with '/'
    if (request->abs_path[path_len - 1] == '/') {
        char *postfix = "index.html";
        response->content_type = html_mime;

        size_t postfix_len = strlen(postfix);
        char *extended_path = malloc(sizeof(char) *
                                     (path_len + postfix_len + 1));
        strcpy(extended_path, request->abs_path);
        strcpy(extended_path + path_len, postfix);
        file = lookup_entry(extended_path);
        free(extended_path);
    } else
        // URL path could be a file
        file = lookup_entry(request->abs_path);

    // If there is no such file in the storage
    if (file == NULL) {
        response->status_line = not_found;
        response->content_type = html_mime;
        file = lookup_entry(DEFAULT_ERROR_404_FILE);
    } else {
        // found file in the storage
        response->status_line = ok;

        // search mime type
        if (response->content_type == default_mime) {
            char *attr = file->path + request->last_dot_in_path;

            for (size_t i = 0; i < mime_types_len; ++i) {
                if (strcmp(attr, mime_types[i].attr) == 0) {
                    response->content_type = mime_types[i].mime;
                    break;
                }
            }
        }
    }

    // So there is a file (can be error page)
    if (file != NULL) {
        response->body = file->content;
        response->body_length = file->content_length;
    } else {
        // Fallback error page (there is no error page too)
        response->status_line = not_found;
        response->content_type = html_mime;
        response->body = fallback_404_page;
        response->body_length = strlen(response->body);
    }
}

void init_response_factory(void) {
    storage = get_storage();
    storage_size = get_storage_size();
}
