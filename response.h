/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Response logic
 *
 * Factory of HTTP responses. It's an abstraction in a way.
 *
 */

#ifndef MOTH_RESPONSE_H
#define MOTH_RESPONSE_H

#include "http_structs.h"

/// Storage of webroot's files
extern file_storage_entry const *storage;

/// Element counter of the storage
extern size_t storage_size;

/**
 * Set storage pointers
 */
void init_response_factory(void);

/**
 * Makes a response from a request
 *
 * @param request HTTP request
 * @param response HTTP response
 */
void process_request(const http_request *request, http_response *response);

#endif //MOTH_RESPONSE_H
