/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Receive, parse and send data through the network (implementation)
 *
 */

#include "server.h"
#include "response.h"

#include<string.h>
#include<stdbool.h>
#include<stdlib.h>
#include<string.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<time.h>
#include<ctype.h>
#include <netdb.h>
#include <syslog.h>
#include <stdio.h>
#include <errno.h>

int server_listen_socket;

/**
 * Open a TCP socket
 *
 * @param port Port to bind
 * @param do_reuse Address reuse allowed?
 */
void open_socket(const char *port, int do_reuse) {
    struct addrinfo hints;
    struct addrinfo *response;
    struct addrinfo *p;

    int socket_descriptor = -1;

    // getaddrinfo for host
    memset (&hints, 0, sizeof(hints));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    if (getaddrinfo(NULL, port, &hints, &response) != 0) {
        syslog(LOG_ERR, "getaddrinfo() error: %s\n", strerror(errno));
        exit(1);
    }
    // socket and bind
    for (p = response; p != NULL; p = p->ai_next) {
        socket_descriptor = socket(p->ai_family, p->ai_socktype, 0);

        if (socket_descriptor == -1)
            continue;

        if (setsockopt(socket_descriptor, SOL_SOCKET, SO_REUSEADDR, &do_reuse, sizeof(&do_reuse)))
            continue;

        if (bind(socket_descriptor, p->ai_addr, p->ai_addrlen) == 0)
            break;
    }

    if (p == NULL) {
        syslog(LOG_ERR, "could not bind address: %s\n", strerror(errno));
        exit(1);
    }

    freeaddrinfo(response);

    // listen for incoming connections
    if (listen(socket_descriptor, 1000000) != 0) {
        syslog(LOG_ERR, "listen() error: %s\n", strerror(errno));
        exit(1);
    }

    server_listen_socket = socket_descriptor;
}

void accept_connections(void) {
    http_request request;
    http_response response;

    char request_buffer[REQUEST_BUFFER_LENGTH];

    // ACCEPT connections
    while (1) {
        const int socket = accept(server_listen_socket, NULL, NULL);
        if (socket < 0)
            return; // Can caused by halting of the mother socket

        // fill with zeroes
        memset((void *) request_buffer, (int) '\0', REQUEST_BUFFER_LENGTH);

        int receive_status = (int) recv(socket,
                                        request_buffer,
                                        REQUEST_BUFFER_LENGTH - 1,
                                        0);

        if (receive_status < 0)
            invalid_response_factory(RECEIVE_ERROR, &response);
        else if (receive_status == 0)
            invalid_response_factory(UNEXPECTEDLY_CLOSED, &response);
        else {
            req_parse_status status = parse_request(request_buffer, &request);

            if (status == VALID_REQUEST)
                process_request(&request, &response);
            else
                invalid_response_factory(status, &response);
        }

        write_response(socket, &response);

        // Closing SOCKET
        // All further send and receive operations are DISABLED...
        shutdown(socket, SHUT_RDWR);
        close(socket);
    }
}

req_parse_status parse_request(const char *buffer, http_request *request) {
    req_parse_state state = READ_HTTP_METHOD;
    bool processing = true;
    size_t pos = 0; // index of processed character
    char *path; // for string processing
    request->last_dot_in_path = -1;

    const char *get_ = "GET ";
    const char *http_09 = "HTTP/0.9";
    const char *http_10 = "HTTP/1.0";
    const char *http_11 = "HTTP/1.1";

    /**
     * Request parser state machine
     */
    while (processing) {
        if (buffer[pos] == '\r') {
            pos++;
            continue;
        }

        switch (state) {
            case READ_HTTP_METHOD:
                // get, GET, GeT, etc. what about RFC?
                if (strncmp(buffer + pos, get_, strlen(get_)) == 0) {
                    request->method = HTTP_GET;
                    pos += strlen(get_);
                } else {
                    return UNSUPPORTED_HTTP_METHOD;
                }
                state = READ_HTTP_PATH_ONLY; // set next state
                break;
            case READ_HTTP_PATH_ONLY:
                // A server SHOULD return 414 (Request-URI Too Long) status if a URI is longer
                // than the server can handle (see section 10.4.15).
                path = request->abs_path;
                int path_length = 0;

                // It's a simple path (no schema and domain)
                if (buffer[pos] != '/')
                    return BAD_REQUEST;

                *path++ = buffer[pos++]; // Read to the path
                path_length++;

                // while the processing in the path part
                // e.g. GET /path/file.attr HTTP/1.1
                while (buffer[pos] != ' ') {
                    if (path_length == MAX_PATH_LENGTH)
                        return PATH_TOO_LONG;

                    // I'm lazy to do sophisticated URL validator
                    // If msg char is a quasi-printable character that is OK
                    if (' ' < buffer[pos] && buffer[pos] <= 'z') {
                        *path++ = buffer[pos++];
                        path_length++;
                        if (buffer[pos] == '.')
                            request->last_dot_in_path = path_length;
                    } else
                        return BAD_REQUEST;
                }
                *path = '\0'; // terminate string
                pos++; // Skip ' '
                state = READ_HTTP_VERSION; // set next state
                break;
            case READ_HTTP_VERSION:

                if (strncmp(buffer + pos, http_11, strlen(http_11)) == 0) {
                    pos += strlen(http_11);
                } else if (strncmp(buffer + pos, http_10, strlen(http_10))
                           == 0) {
                    pos += strlen(http_10);
                } else if (strncmp(buffer + pos, http_09, strlen(http_09))
                           == 0) {
                    pos += strlen(http_09);
                } else
                    return UNKNOWN_PROTO;

                // Only process GET requests
                // host header is not processed
                // so it is a simple request processor
                // got all information that is needed
                processing = false;
                break;
        }
    }

    return VALID_REQUEST;
}

/**
 * Write http response to the socket
 *
 * @param socket Socket descriptor
 * @param response Response to write
 */
void write_response(const int socket, const http_response *response) {
    // TODO overflow warning!
    char buffer[HEADER_BUFFER_LENGTH];
    char *pbuff = (char *) buffer;

    /**
     * Write http version and status line
     */
    if (response->version == HTTP_VER_11)
        pbuff += sprintf(pbuff, "HTTP/1.1 %s\n", response->status_line);
    else
        pbuff += sprintf(pbuff, "HTTP/1.0 %s\n", response->status_line);

    /**
     * Write date and server lines
     */
    time_t timer;
    time(&timer);
    struct tm *tm_info = localtime(&timer);

    // e.g. Sun, 18 Sept 2018 10:36:20
    // 60 is a superior approximation for the length of the date header's length
    pbuff += strftime(pbuff, 60, "Date: %a, %d %b %Y %H:%M:%S %Z\n", tm_info);

    pbuff += sprintf(pbuff, "Server: Moth 0.2\n");

    /**
     * Write other headers
     */

    /**
     * Write Content-Length header
     */
    if (response->body_length > 0)
        pbuff += sprintf(pbuff, "Content-Length: %ld\n", response->body_length);

    pbuff += sprintf(pbuff, "Connection: Closed\n");
    pbuff += sprintf(pbuff, "Content-Type: %s; charset=utf-8\n\n",
                     response->content_type);
    // No end of string needed (\0)

    /**
     * Send header
     */
    size_t buffer_len = pbuff - buffer;
    write(socket, buffer, buffer_len);

    /**
     * Send body (if there is a body)
     */
    if (response->body_length > 0) {
        write(socket, response->body, response->body_length);
    }
}

void invalid_response_factory(const req_parse_status status,
                              http_response *response) {
    const char *bad_request = "400 Bad Request";
    const char *unknown_proto = "408 Unsupported HTTP proto";
    const char *unsupported_method = "408 Unsupported HTTP method";
    const char *path_too_long = "414 Request-URI Too Long";

    // Fill required elements of the struct
    response->version = HTTP_VER_11;

    switch (status) {
        case BAD_REQUEST:
            response->status_line = bad_request;
            break;
        case UNSUPPORTED_HTTP_METHOD:
            response->status_line = unsupported_method;
            break;
        case UNKNOWN_PROTO:
            response->status_line = unknown_proto;
            break;
        case RECEIVE_ERROR:
            response->status_line = bad_request; // TODO think about this
            break;
        case PATH_TOO_LONG:
            response->status_line = path_too_long;
            break;
        case UNEXPECTEDLY_CLOSED: // nothing to do
            break;
        case VALID_REQUEST:
            break; // satisfy code inspection (just placeholder)
    }

    //response->body = response->status_line;
    //response->body_length = strlen(response->body);
}

void close_socket(void) {
    // Closing SOCKET
    // All further send and receive operations are DISABLED...
    shutdown(server_listen_socket, SHUT_RDWR);
    close(server_listen_socket);
}
