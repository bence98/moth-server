/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Receive, parse and send data through the network.
 *
 */

#ifndef MOTH_SERVER_H
#define MOTH_SERVER_H

#include "http_structs.h"

/// TCP socket's descriptor
extern int server_listen_socket;

/**
 * Terminate server's socket
 */
void close_socket(void);

/**
 * Open a TCP socket on a port
 *
 * @param port Port to bind
 * @param do_reuse Address reuse allowed?
 * @return File descriptor of the socket
 */
void open_socket(const char *, int);

/**
 * States of the parse_request method's state machine
 */
typedef enum req_parse_state {
    /// Read method part of the request
            READ_HTTP_METHOD,

    /// Read relative-path part of the request
            READ_HTTP_PATH_ONLY,

    /// Read http version part of the request
            READ_HTTP_VERSION,
} req_parse_state;

/**
 * States of request parsing
 */
typedef enum req_parse_status {
    /// Successfully parsed request
            VALID_REQUEST = 0,

    /// Cannot parse request
            BAD_REQUEST,

    /// Invalid HTTP request version
            UNKNOWN_PROTO,

    /// recv() error
            RECEIVE_ERROR,

    /// Client disconnected unexpectedly
            UNEXPECTEDLY_CLOSED,

    /// Funny URL
            PATH_TOO_LONG,

    /// Unsupported HTTP verb
            UNSUPPORTED_HTTP_METHOD
} req_parse_status;

/**
 * Parse request's data
 *
 * @param buffer Buffer to process on
 * @param request Output of the parsing
 * @return State constant (e.g. VALID_REQUEST) [see: request_parse_status]
 */
req_parse_status parse_request(const char *buffer, http_request *request);

/**
 * Write response to the socket
 *
 * @param socket Socket descriptor to process on
 * @param response Valid response
 */
void write_response(int socket, const http_response *response);

/**
 * Create a response to an invalid request
 *
 * @param status Invalid request's status
 * @param response Created response
 */
void invalid_response_factory(req_parse_status status,
                              http_response *response);

/**
 * Handle requests uses request processor in response.h
 */
void accept_connections(void);

// TODO what about unicode?
// TODO url_encode()

#endif //MOTH_SERVER_H
