/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Storage of webroot's files (implementation)
 *
 */

#include "storage.h"
#include "config.h"

#include<stdio.h>
#include<string.h>

file_storage_entry *_storage;
size_t _storage_len;

/**
 * Get webroot's file-list counter
 * @param webroot The webroot
 * @return Number of files; -1 if an error has occurred
 */
int get_file_num(const char *webroot) {
    char *count_cmd_format = "find -L %s -type f | wc -l";
    const size_t cmd_len = CONFIG_VALUE_BUFFER_LENGTH +
                           strlen(count_cmd_format) + 1;
    char total_cmd[cmd_len];
    sprintf(total_cmd, count_cmd_format, webroot);

    FILE *result;
    int file_num = 0;
    // Open the command for reading
    result = popen(total_cmd, "r");
    if (result == NULL) {
        return -1;
    }
    fscanf(result, "%d", &file_num);
    pclose(result);

    return file_num;
}

/**
 * Get webroot's file-list
 *
 * @param webroot The webroot
 * @param file_num Number of files
 * @return List of files (in absolute path format)
 */
FILE *get_file_list(const char *webroot, const int file_num) {
    char *list_cmd_format = "find -L %s -type f";
    const size_t cmd_len = CONFIG_VALUE_BUFFER_LENGTH +
                           strlen(list_cmd_format) + 1;
    char total_cmd[cmd_len];
    sprintf(total_cmd, list_cmd_format, webroot);

    // Allocate file_entry array
    _storage = malloc(sizeof(file_storage_entry) * file_num);

    sprintf(total_cmd, list_cmd_format, webroot);
    // Open the command for reading
    return popen(total_cmd, "r");
}

/**
 * Read a file's content
 * @param path Path of the file
 * @param content Content output
 * @param content_len Length output
 */
void read_file_content(const char *path, char **content, size_t *content_len) {
    FILE *f = fopen(path, "rb");

    if (f == NULL) {
        *content_len = 0;
        *content = NULL;
        return;
    }

    fseek(f, 0, SEEK_END);
    *content_len = (size_t) ftell(f);
    fseek(f, 0, SEEK_SET);  //same as rewind(f);

    *content = (char *) malloc(sizeof(char) * (*content_len + 1));
    fread(*content, *content_len, 1, f);
    fclose(f);

    (*content)[*content_len] = '\0';
}

/**
 * Compares two file_storage_entry to determine their order.
 * @param ptr1 Pointer of the first file_storage_entry
 * @param ptr2 Pointer of the second file_storage_entry
 * @return Order
 */
int storage_comperer(const void *ptr1, const void *ptr2) {
    file_storage_entry const *entry1 = ptr1;
    file_storage_entry const *entry2 = ptr2;

    return strcmp(entry1->path, entry2->path);
}

read_storage_state read_storage(const char *webroot) {
    int file_num = get_file_num(webroot);

    if (file_num == -1)
        return CANNOT_LIST_STORAGE_FOLDER;
    else if (file_num <= 0)
        return STORAGE_NO_FILES;
    _storage_len = (size_t) file_num;

    FILE *result = get_file_list(webroot, file_num);
    if (result == NULL) {
        return CANNOT_LIST_STORAGE_FOLDER;
    }

    file_storage_entry *storage = _storage;

    const size_t webroot_len = strlen(webroot);

    char path[MAX_PATH_LENGTH];
    for (int i = 0; i < file_num; ++i) {
        // internal error; should not happen
        if (fgets(path, MAX_PATH_LENGTH - 1, result) == NULL)
            return MORE_FILE_EXPECTED;

        size_t len = strlen(path);
        path[len - 1] = '\0';

        read_file_content(path, &storage[i].content,
                          &storage[i].content_length);

        if (storage[i].content == NULL)
            return CANNOT_READ_FILE;

        storage[i].path = malloc(sizeof(char) * len);
        // skip webroot from path
        strcpy(storage[i].path, path + webroot_len);
    }
    pclose(result);

    // ability to search use bsearch()
    qsort(storage, (size_t) file_num, sizeof(file_storage_entry),
          storage_comperer);

    return STORAGE_OK;
}

file_storage_entry const *get_storage() {
    return _storage;
}

size_t get_storage_size() {
    return _storage_len;
}

void free_storage(void) {
    for (size_t i = 0; i < _storage_len; ++i) {
        file_storage_entry *entry = _storage + i;

        free(entry->path);
        free(entry->content);
    }
    free(_storage);
}
