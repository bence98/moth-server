/*
 * ███╗   ███╗ ██████╗ ████████╗██╗  ██╗
 * ████╗ ████║██╔═══██╗╚══██╔══╝██║  ██║
 * ██╔████╔██║██║   ██║   ██║   ███████║
 * ██║╚██╔╝██║██║   ██║   ██║   ██╔══██║
 * ██║ ╚═╝ ██║╚██████╔╝   ██║   ██║  ██║
 * ╚═╝     ╚═╝ ╚═════╝    ╚═╝   ╚═╝  ╚═╝
 *             WEB Server
 */

/**
 * @file
 * @brief Storage of webroot's files
 *
 */

#ifndef MOTH_SERVER_STORAGE_H
#define MOTH_SERVER_STORAGE_H

#include<stdlib.h>

/**
 * A file in webroot that can be served
 */
typedef struct file_storage_entry {
    /// Path of the file
    char *path;

    /// Length of the content
    size_t content_length;

    /// Content itself
    char *content;
} file_storage_entry;

/**
 * States of the parse_request method's state machine
 */
typedef enum read_storage_state {
    /// Successfully read files
            STORAGE_OK,

    /// List command has failed
            CANNOT_LIST_STORAGE_FOLDER,

    /// There were no file in the webroot
            STORAGE_NO_FILES,

    /// internal error; should not happen
            MORE_FILE_EXPECTED,

    /// Cannot read storage file
            CANNOT_READ_FILE
} read_storage_state;

/**
 * Read files to memory
 *
 * @param working_dir Root of the files
 * @return Status of the reading
 */
read_storage_state read_storage(const char *working_dir);

/**
 * Free storage files
 */
void free_storage(void);

/// Array of stored files
extern file_storage_entry *_storage;

/// Number of stored files
extern size_t _storage_len;

/// Getter for storage files
file_storage_entry const *get_storage(void);

/// Getter for number of the files
size_t get_storage_size(void);

#endif //MOTH_SERVER_STORAGE_H
