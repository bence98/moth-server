from locust import HttpLocust, TaskSet, task


class IndexPage(TaskSet):
    @task
    def index_assertion(self):
        r = self.client.get("/")
        assert r.status_code is 200, "Unexpected response code: " + r.status_code


class WebsiteUser(HttpLocust):
    task_set = IndexPage
    host = "http://127.0.0.1:8081"
    min_wait = 1000
    max_wait = 5000
