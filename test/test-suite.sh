#!/bin/sh

##################### Config ###################################

HOST="127.0.0.1:8081"

##################### Methods ###################################

# $CI is an external variable created by GitLab CI
if [ -z $CI ]; then
	USE_COLORS="y"
else
	USE_COLORS="n"
fi

if [ $USE_COLORS = "y" ]; then
	RED=$(tput setaf 1)
	GREEN=$(tput setaf 2)
	NORMAL=$(tput sgr0)
fi

# Prints passed-style message line
# First param: message
test_ok() {
	if [ $USE_COLORS = "y" ]; then
		printf '%-70s%10s%s\n' "$1 $GREEN" "[ PASSED ]" "$NORMAL"
	else
		printf "$1\t\t[ OK ]\n"
	fi
}

# Prints failed-style message line
# First param: message
test_failed() {
	if [ $USE_COLORS = "y" ]; then
		printf '%-70s%10s%s\n' "$1 $RED" "[ FAILED ]" "$NORMAL"
	else
		printf "$1\t\t[ FAILED ]\n"
	fi
	echo "!!! More tests are cancelled !!!"
	exit 1
}

# Get body of a GET request
# First param: PATH of the URL
http_get_body() {
	curl -sS 127.0.0.1:8081$1
}

# Get headers of a GET request
# First param: PATH of the URL
http_get_headers() {
	curl -sSIX GET 127.0.0.1:8081$1 2>&1
}

# Get headers and body of a GET request (contains some CURL message)
# First param: PATH of the URL
http_get_verbose() {
	curl -sv 127.0.0.1:8081$1 2>&1
}

# First param: PATH of the URL
# Second param: sting to check
check_body_contains() {
	CONTENT=$(http_get_body "$1")
	case "$CONTENT" in
		*"$2"*) test_ok "Check body contains \"$2\" on $1" ;;
		*) test_failed "Check body contains \"$2\" on $1"
	esac
}

# First param: PATH of the URL
# Second param: sting to check
check_body_not_contains() {
	CONTENT=$(http_get_body "$1")
	case "$CONTENT" in
		*"$2"*) test_failed "Check body NOT contains \"$2\" on $1" ;;
		*) test_ok "Check body NOT contains \"$2\" on $1"
	esac
}

# First param: PATH of the URL
# Second param: sting to check
check_headers_contains() {
	CONTENT=$(http_get_headers "$1")
	case "$CONTENT" in
		*"$2"*) test_ok "Check headers contain \"$2\" on $1" ;;
		*) test_failed "Check headers contains \"$2\" on $1"
	esac
}

# First param: PATH of the URL
# Second param: sting to check
check_headers_not_contains() {
	CONTENT=$(http_get_headers "$1")
	case "$CONTENT" in
		*"$2"*) test_failed "Check headers NOT contain \"$2\" on $1" ;;
		*) test_ok "Check headers NOT contain \"$2\" on $1"
	esac
}

######################## Tests ################################

check_headers_not_contains / "Failed to connect"

check_headers_contains / "200 OK"
check_headers_contains / "Date: "
check_headers_contains / "Server: Moth"
check_headers_contains / "Content-Length: "
check_headers_contains / "Content-Type: "
check_headers_contains / "Connection: "

check_body_contains / "<!DOCTYPE html>"

check_headers_contains /error "404 NOT FOUND"
check_headers_contains /error "Date: "
check_headers_contains /error "Server: Moth"
check_headers_contains /error "Content-Length: "
check_headers_contains /error "Content-Type: "
check_headers_contains /error "Connection: "

check_body_contains /error "<!DOCTYPE html>"

######################## End ################################
exit 0
